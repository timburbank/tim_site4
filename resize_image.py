#!/usr/bin/python3
'''
Generate thumbnail and post header image sizes
'''

import os
import argparse
from PIL import Image

import logging

logging.basicConfig(
   format='%(levelname)s %(funcName)s %(lineno)s: %(message)s', 
   level=logging.DEBUG)


def resize_image(
    image_path, 
    out_dir, 
    post_size=(680, 200), 
    suffix='',
    crop_top=0.2):
    '''
    Output thumbnail and header sized images. New images are 
    saved as JPEG with _post.jpg and _thumb.jpg suffixes

    Args:
        image_path (str): Path to source image
        out_dir (str): Ouput dir to put new images
        post_size (tuple): x/y pixel size of post image
        thumb_size (tuple): x/y pixel size of thumbnail
        crop_top (float): Proportion of image to crop out on top
        thumb_crop_top (int): pixels from top of image to crop thumbnail
    '''
    
    ## Post size
    try:
        image = Image.open(image_path)
    except IOError:
        raise IOError(image_path + ' is not an image or cannot be read')

    ## We don't care about y direction yet, so just make the upper limit big?
    image.thumbnail([post_size[0], 2000], Image.ANTIALIAS)


    if image.size[1] > post_size[1]:# and False:
        top = image.size[1] * crop_top

        logging.info('Cropping {}px down from {}px'.format(post_size[1], top))

        ## Make sure we don't add more pixels by cropping 
        ## past the edge of the image
        if top + post_size[1] > image.size[1]:
            top = image.size[1] - post_size[1]
            logging.debug('Readjusting top to {}'.format(top))

        if top < 0:
            top = 0
            logging.debug('Readjusting top to 0')


        cropped_image = image.crop((
            0,
            top,
            image.size[0],
            top + post_size[1]))

        rgb_image = cropped_image.convert('RGB')

    else:
        rgb_image = image.convert('RGB')        


    post_name = os.path.splitext(os.path.basename(image_path))[0] + suffix + '.jpg'
    post_path = os.path.join(out_dir, post_name)

    try:
        rgb_image.save(post_path, 'JPEG')
    except IOError:
        raise IOError('Could not write ' + post_path)

    logging.info('saved ' + post_path)

def cli_args():
    '''
    Parse command line arguments
    
    Returns:
        args object
    '''
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        'source_image',
        help='Source image')

    parser.add_argument(
        'out_dir',
        help='Destination directory')

    parser.add_argument(
        '--header-top',
        '-t',
        type=float,
        default=0.2,
        help='Proportion of image to crop from top of header size')

    return parser.parse_args()

if __name__ == '__main__':
    args = cli_args()
    resize_image(
        args.source_image, 
        args.out_dir, 
        (680, 200), 
        '_header', 
        crop_top=args.header_top)

    resize_image(
        args.source_image, 
        args.out_dir, 
        (400, 225),
        '_thumb',
        crop_top=args.header_top)




